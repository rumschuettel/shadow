import subprocess as sp
from pretty import pretty

class ExeError(Exception):
  pass

# execute command
def execute(exe, command, args={}):
  if not exe in executables:
    raise ExeError("invalid executable")
  if not command in executables[exe]["commands"]:
    raise ExeError("invalid command")

  try:
    command = exe + " " + executables[exe]["commands"][command] % args
  except KeyError as e:
    raise ExeError("not all arguments given: " + str(e))
  except ValueError as e:
    print(pretty(args, "dark gray"))
    raise ExeError("invalid argument: " + str(e))
  print(pretty(command, "dark gray"))

  return sp.Popen(command, stdout=sp.PIPE, stderr=executables[exe]["stderr"], universal_newlines=True)
# executables
executables = {
  "ffprobe": {
    "stderr": sp.STDOUT,
    "version": None,
    "commands": {
      "version": "-version",
      "analyse": '"%(in)s"',
    }
  },
  "ffmbc": {
    "stderr": sp.STDOUT,
    "version": None,
    "commands": {
      "version": "-version",
      "analyse": '-i "%(in)s"',
      "base": '-i "%(in)s" -vcodec %(vcodec)s -acodec %(acodec)s -threads 0 -y "%(out)s"',
      "rewrap": '-i "%(in)s" -vcodec copy %(vflags)s -acodec copy -threads 0 -y "%(out)s"'
    }
  },
  "ffmpeg": {
    "stderr": sp.STDOUT,
    "version": None,
    "commands": {
      "version": "-version",
      "rewrap": '-i "%(in)s" -vcodec copy -acodec copy -sn -threads 0 -y "%(out)s"',			 
      "transcode": '-i "%(in)s" -codec:v %(vcodec)s %(vflags)s -vf "%(vfilter)s" -codec:a %(acodec)s %(aflags)s -sn -threads 4 -y "%(out)s"'
    }
  },
  "mencoder": {
    "stderr": sp.PIPE,
    "version": None,
    "commands": {
      "version": "-ovc",
      "m701": '"%(in)s" -ffourcc M701 -ovc copy -oac pcm -af pan=2:1:0:0:1:0.5:0.5:1:1:1:0:0:1,format=s16le -force-avi-aspect %(aspect)f -o "%(out)s"'
    }
  }
}

