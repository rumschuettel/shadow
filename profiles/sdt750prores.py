import os

__HERE__ = os.path.dirname(__file__)

def rewrap(factory, path, pong, info):
  out = pong("mov")

  print(info)

  trim = ""
  if info["trim_start"] != None:
    trim += " -ss " + str(info["trim_start"])
  if info["trim_time"] != None:
    trim += " -t " + str(info["trim_time"])

  code = factory("ffmbc", "rewrap", {
    "in": path,
    "vflags": trim, 
    "out": out
  })

  return (code, out)

avs_deshake_estimate_template = """# shadow avs file
LoadVirtualDubPlugin("%(vdplugins)s\\Deshaker.vdf", "Deshaker")
FFMpegSource2("%(in)s")

%(interlaced)s==1 ? AssumeTFF() : last
ConvertToRGB()
Deshaker("18|1|30|4|1|0|1|0|%(width)d|%(height)d|1|2|1000|1000|1000|1000|0.5|0|4|2|8|30|300|4|%(log)s|0|1|%(ignoreLeft)d|%(ignoreRight)d|%(ignoreTop)d|%(ignoreBottom)d|0|0|0|0|0|%(interlaced)d|1|1|15|15|5|15|0|0|30|30|0|0|0|0|1|0|0|10|1000|1|88|1|1|20|5000|100|20|1")
"""
avs_source_template = """#avs input pipe for vdub
FFMpegSource2("%(in)s")

AssumeTFF()
ConvertToRGB32()
"""
def deshake_estimate(factory, path, pong, info):
  print(info)
  if info["proxy"] or (info["depan"]+info["derotate"]+info["dezoom"]) <= 0:
    return (0, path)

  info["logpath"] = logpath = pong("log", True)
  # use this block if for whatever reason deshaker works again in avisynth
  avspath = pong("avs", True)
  avs = open(avspath, "w")
  avs.write(avs_deshake_estimate_template % {
    "vdplugins": __HERE__,
    "in": path,
    "width": info["width"],
    "height": info["height"],

    "log": logpath,
    "ignoreTop": info["ignoreTop"]*info["height"],
    "ignoreRight": info["ignoreRight"]*info["width"],
    "ignoreBottom": info["ignoreBottom"]*info["height"],
    "ignoreLeft": info["ignoreLeft"]*info["width"],
    "interlaced": 1 if info["mode"] == "i" else 0
    })
  avs.close()

  tmpout = pong("avi", True)
  code = factory("ffmpeg", "rewrap", {
    "in": avspath,
    "out": tmpout
    })
  os.unlink(tmpout)
  os.unlink(avspath)

  return (code, path)

from math import log, ceil, floor
def pulldown(fps, new_fps, blend=False):
  fac = new_fps/fps
  skip = floor(1/fac) if not blend else 1
  fac_after_skip = fac*skip
  fps_after_skip = fps/skip
  blend_steps = ceil(log(fac_after_skip)/log(2/3))
  cmd = "SelectEvery(%d,0)" % skip
  for i in range(blend_steps):
    cmd += "ConvertFPS(%f)\n" % (fps_after_skip*fac_after_skip**((i+1)/blend_steps))
  return cmd

avs_transcode_template = """# shadow avs file
%(deshake)s LoadVirtualDubPlugin("%(vdplugins)s\\Deshaker.vdf", "Deshaker")
try {
  FFMpegSource2("%(in)s", atrack=-1)
}
catch (error) {
  FFMpegSource2("%(in)s")
}

%(deinterlacing)s AssumeTFF()
%(deshake)s ConvertToRGB()
%(deshake)s Deshaker("18|2|30|4|1|0|1|0|%(width)d|%(height)d|1|2|%(depanX)d|%(depanY)d|%(derotate)d|%(dezoom)d|0.5|1|3|2|8|30|300|4|%(log)s|0|1|0|0|0|0|0|0|0|0|0|%(interlaced)d|1|1|30|30|20|50|0|0|30|30|0|0|0|0|1|0|0|10|1000|1|88|1|1|20|5000|100|20|1")
%(deshake)s ConvertToYV12()

# if deshaker used, only bob, otherwise qtgmc
HQdeinterlace=%(qtgmc)d
%(deshake)s HQdeinterlace=0
%(deinterlacing)s HQdeinterlace==1 ? QTGMC(preset="faster") : Bob()
AssumeFPS(%(fps_p)f)

%(retarget)s SetMemoryMax(1024)
%(retarget)s LoadPlugin("C:\Program Files (x86)\Avisynth 2.6\plugins\svpflow1.dll")
%(retarget)s LoadPlugin("C:\Program Files (x86)\Avisynth 2.6\plugins\svpflow2.dll")
%(retarget)s Import("C:\Program Files (x86)\Avisynth 2.6\plugins\InterFrame2.avsi")
%(retarget)s InterFrame(Cores=1, Tuning="Film", NewNum=Round(%(fps_p)f/%(speed)f*1000), NewDen=1001)

%(retarget)s AssumeFPS(%(new_fps)f)
%(retarget)s ResampleAudio(48000)

%(pulldown_cmd)s
%(pulldown)s AssumeFPS(%(new_fps)f)
%(pulldown)s ResampleAudio(48000)

%(sharpen)s ConvertToYV12()
%(sharpen)s LSFmod(defaults="fast", strength=%(sharpen_strength)d, soothe=false)
"""
def transcode(factory, path, pong, info):
  vf = "scale=%d:-1"
  fps_p = 2*info["fps"] if info["mode"] == "i" else info["fps"]
  new_fps = info["new_fps"] or fps_p

  # low-quality proxy
  if info["proxy"]:
    vf = vf % int(info["width"]/2)
    # low-quality deinterlacing for proxy files
    if info["mode"] == "i":
      vf += ",yadif=1:-1" # outputs one frame for each field, so target becomes twice the framerate 
    if info["new_fps"] != fps_p:
      vf += ",setpts=%f*PTS" % (1/info["speed"])

    avspath = False

  # high-quality avisynth
  else:
    vf = vf % info["width"]

    avspath = pong("avs", True)
    avs = open(avspath, "w")
    avs.write(avs_transcode_template % {
      "vdplugins": __HERE__,
      "in": path,
      "interlaced": 1 if info["mode"] == "i" else 0,
      "deinterlacing": "" if info["mode"] == "i" else "#",
      "fps": info["fps"],
      "fps_p": fps_p,
      "width": info["width"],
      "height": info["height"],

      "log": info["logpath"] if "logpath" in info else "",
      "deshake": "" if (info["depan"]+info["dezoom"]+info["derotate"]) > 0 else "#",
      "depanX": info["depanX"] or info["depan"],
      "depanY": info["depanY"] or info["depan"],
      "dezoom": info["dezoom"],
      "derotate": info["derotate"],

      "qtgmc": 1 if info["qtgmc"] else (0 if info["qtgmc"]==None and info["speed"] > 4 else 1),

      "retarget": "" if (new_fps/info["speed"] > fps_p) else "#",
      "pulldown": "" if (new_fps/info["speed"] < fps_p) else "#",
      "pulldown_cmd": pulldown(fps_p, new_fps/info["speed"], info["speed"]>1) if (new_fps/info["speed"] < fps_p) else "",
      "new_fps": new_fps,
      "speed": info["speed"],

      "sharpen": "" if info["sharpen"] > 0 else "#",
      "sharpen_strength": info["sharpen"]
    })
    avs.close()
    path = avspath

  out = pong("mov")
  code = factory("ffmpeg", "transcode", {
    "in": path,
    "vcodec": "prores",
    "vflags": "-profile:v %d -r %f" % ((0 if info["proxy"] else 2), new_fps),
    "vfilter": vf,
    "acodec": "aac -strict -2",
    "aflags": "-b:a 256k -ac 2 -ar 48000 -cutoff 20000",
    "out": out
  })

  # clean up
  if avspath:
    os.unlink(avspath)
  if "logpath" in info:
    os.unlink(info["logpath"])

  return (code, out)

profile = {
  "defaults": {
    "depan": 10000,
    "depanX": None,
    "depanY": None,
    "dezoom": 1000,
    "derotate": 3000,

    "ignoreTop": 0,
    "ignoreRight": 0,
    "ignoreBottom": 0,
    "ignoreLeft": 0,

    "new_fps": 29.97,
    "speed": 1,

    "qtgmc": None,
    "sharpen": 500,

    "trim_start": None,
    "trim_time": None,

    "proxy": False
  },
  "stage": [{
    "f": rewrap
  }, {
    "f": deshake_estimate
  }, {
    "f": transcode
  }]
}
