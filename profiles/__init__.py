import os
import glob
import sys

__HERE__ = os.path.dirname(__file__)

profiles = {}
sys.path.append(__HERE__)

for path in glob.glob(__HERE__+"/*.py"):
	name = os.path.splitext(os.path.basename(path))[0]
	if name == "__init__":
		continue

	try:
		profiles[name] = __import__(name).profile
	except Exception as e:
		raise
