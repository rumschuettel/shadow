import os

__HERE__ = os.path.dirname(__file__)

def rewrap(factory, path, pong, info):
  out = pong("mov")
  code = factory("ffmbc", "rewrap", {"in": path, "out": out})

  return (code, out)

avs_deshake_estimate_template = """# shadow avs file
LoadVirtualDubPlugin("%(vdplugins)s\\Deshaker.vdf", "Deshaker")
FFMpegSource2("%(in)s")

AssumeTFF()
ConvertToRGB32()
Deshaker("18|1|30|4|1|0|1|0|%(width)d|%(height)d|1|2|10000|10000|0|3000|4|0|6|2|2|30|0|4|%(log)s|0|0|0|0|0|0|0|0|0|0|0|%(interlaced)d|1|1|50|50|15|15|0|0|30|30|0|0|1|0|1|0|0|10|1000|1|80|1|1|20|5000|100|5|0")
"""
avs_source_template = """#avs input pipe for vdub
FFMpegSource2("%(in)s")

AssumeTFF()
ConvertToRGB32()
"""
def deshake_estimate(factory, path, pong, info):
  print(info)
  if info["proxy"] or (info["depan"]+info["derotate"]+info["dezoom"]) <= 0:
    return (0, path)

  info["logpath"] = logpath = pong("log", True)
  # use this block if for whatever reason deshaker works again in avisynth
  avspath = pong("avs", True)
  avs = open(avspath, "w")
  avs.write(avs_deshake_estimate_template % {
    "vdplugins": __HERE__,
    "in": path,
    "width": info["width"],
    "height": info["height"],
    "log": logpath,
    "interlaced": 1 if info["mode"] == "i" else 0
    })
  avs.close()

  tmpout = pong("avi")
  code = factory("ffmpeg", "rewrap", {
    "in": avspath,
    "out": tmpout
    })
  os.unlink(tmpout)
  os.unlink(avspath)

  return (code, path)

avs_transcode_template = """# shadow avs file
%(deshake)s LoadVirtualDubPlugin("%(vdplugins)s\\Deshaker.vdf", "Deshaker")
try {
  FFMpegSource2("%(in)s", atrack=-1)
}
catch (error) {
  FFMpegSource2("%(in)s")
}

%(deshake)s ConvertToRGB32()
%(deshake)s Deshaker("18|2|30|4|1|0|1|0|%(width)d|%(height)d|1|2|%(strength_p)d|%(strength_p)d|%(strength_r)d|%(strength_z)d|4|1|6|2|2|30|0|4|%(log)s|0|0|0|0|0|0|0|0|0|0|0|%(interlaced)d|1|1|50|50|15|15|0|0|30|30|0|0|1|0|1|0|0|10|1000|1|80|1|1|20|5000|100|5|0")
%(deshake)s ConvertToYV12()

AssumeTFF()
%(deinterlacing)s QTGMC(preset="faster")

AssumeFPS(59.94)
%(retarget)s SetMemoryMax(1024)
%(retarget)s threads=8
%(retarget)s SetMTMode(2, threads)
%(retarget)s LoadPlugin("C:\Program Files (x86)\Avisynth+\plugins\svpflow1.dll")
%(retarget)s LoadPlugin("C:\Program Files (x86)\Avisynth+\plugins\svpflow2.dll")
%(retarget)s Import("C:\Program Files (x86)\Avisynth+\plugins\InterFrame2.avsi")
%(retarget)s InterFrame(Cores=threads, Tuning="Film", NewNum=%(fps)d, NewDen=1001)

%(retarget)s AssumeFPS(%(fps_header)d, true)
%(retarget)s ResampleAudio(48000)
"""
def transcode(factory, path, pong, info):
  vf = "scale=%d:-1"

  # low-quality proxy
  if info["proxy"]:
    vf = vf % int(info["width"]/2)
    # low-quality deinterlacing for proxy files
    if info["mode"] == "i":
      vf += ",yadif=1:-1" # outputs one frame for each field, so target becomes 60p

    avspath = False

  # high-quality avisynth
  else:
    vf = vf % info["width"]

    avspath = pong("avs", True)
    avs = open(avspath, "w")
    avs.write(avs_transcode_template % {
      "vdplugins": __HERE__,
      "in": path,
      "interlaced": 1 if info["mode"] == "i" else 0,
      "deinterlacing": "" if info["mode"] == "i" else "#",
      "width": info["width"],
      "height": info["height"],
      "log": info["logpath"] if "logpath" in info else "",
      "deshake": "" if (info["depan"]+info["dezoom"]+info["derotate"]) > 0 else "#",
      "strength_p": info["depan"],
      "strength_z": info["dezoom"],
      "strength_r": info["derotate"],
      "retarget": "" if info["fps"] != False else "#",
      "fps_header": info["fps_header"],
      "fps": info["fps"] * 1000
    })
    avs.close()
    path = avspath

  out = pong("avi")
  code = factory("ffmpeg", "transcode", {
    "in": path,
    "vcodec": "mpeg2video",
    "vflags": "-intra -q:v %d -pix_fmt yuv422p -qmin 1" % 10 if info["proxy"] else 1,
    "vfilter": vf,
    "acodec": "pcm_s16le",
    "aflags": "",
    "out": out
  })

  # clean up
  if avspath:
    os.unlink(avspath)
  if "logpath" in info:
    os.unlink(info["logpath"])

  return (code, out)

def wrapup(factory, path, pong, info):
  out = pong("avi")
  code = factory("mencoder", "m701", {"in": path, "out": out, "aspect": info["width"]/info["height"]})

  return (code, out)

profile = {
  "defaults": {
    "depan": 10000,
    "dezoom": 3000,
    "derotate": 3000,

    "fps": False,
    "fps_header": 59.94,

    "proxy": True
  },
  "stage": [{
    "f": rewrap
  }, {
    "f": deshake_estimate
  }, {
    "f": transcode
  }, {
    "f": wrapup
  }]
}
