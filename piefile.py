import pickle
import os
import shutil
import copy

# helper functions
def list_merge(a, b):
  if not isinstance(a, list) or not isinstance(b, list):
    return None
  return list(set(a + b))

def dict_merge(a, b):
  if not isinstance(a, dict) or not isinstance(b, dict):
    return None
  result = copy.deepcopy(a)
  for k, v in b.items():
    if k in result:
      if isinstance(result[k], dict):
        result[k] = dict_merge(result[k], v)
      elif isinstance(result[k], list):
        result[k] = list_merge(result[k], v)
    else:
      result[k] = copy.deepcopy(v)
  return result

# standard filename
_piefilename = "piefile"

# piefile version
VERSION = .37

# piefile structure
_pie = {
    "version": VERSION,
    "pongs": [],
    "target": "./",
    "collections": {}
    }

class PieError(Exception):
  pass

# piefile class
class Piefile:
  _pie = None

  def __init__(self, new=False):
    # create new
    if new:
      if os.path.isfile(_piefilename):
        raise PieError("there is already a project in this folder")
      self._pie = _pie
      return

    # load from pickled file
    try:
      f = open(_piefilename, "rb")
      self._pie = pickle.load(f)
      f.close()
    except (OSError, pickle.PickleError) as e:
      raise PieError("could not open project piefile. run with --help") from None

    if self._pie["version"] < VERSION:
      # backwards compatibility
      if self._pie["version"] <= .6:
        for name in self._pie["collections"]:
          if self._pie["collections"][name]["profile"] == "sdt750":
            self._pie["collections"][name]["profile"] = "sdt750intra"

      self._pie = dict_merge(self._pie, _pie)
      self._pie["version"] = VERSION
      print("upgraded piefile. backup saved.")
      shutil.copyfile(_piefilename, _piefilename + ".old")

  def __del__(self):
    if self._pie:
      try:
        f = open(_piefilename, "wb")
        pickle.dump(self._pie, f)
      except (IOError, pickle.PickleError) as e:
        raise PieError("could not save project piefile.")
      finally:
        f and not f.closed and f.close()

  # get piefile attributes
  def get(self, what):
    try:
      return self._pie[what]
    except KeyError as e:
      raise PieError("piefile does not store " + str(e))

  # temporary directories
  def addPong(self, path):
    path = os.path.normpath(path)
    if not os.path.exists(path):
      raise PieError("temporary path " + path + " does not exist.")
    if not path in self._pie["pongs"]:
      self._pie["pongs"].append(path)

  def removePong(self, path):
    try:
      self._pie["pongs"].remove(os.path.normpath(path))
    except ValueError:
      raise PieError("path not in list")

  # profile management
  def setProfile(self, name, profile):
    if not name in self._pie["collections"]:
      raise PieError("invalid collection")

    self._pie["collections"][name]["profile"] = profile

  # profile arguments
  def setOptions(self, name, options, file):
    if not name in self._pie["collections"]:
      raise PieError("invalid collection")

    # try to match file
    if file:
      found = None
      for path in self._pie["collections"][name]["paths"]:
        if file in path:
          if found:
            raise PieError("multiple paths in collection found, be more specific")
          found = path
      if not found:
        raise PieError("file not in collection")
      file = found

    if not file:
      pieopts = self._pie["collections"][name]["options"]
    else:
      pieopts = self._pie["collections"][name]["paths"][file]

    adds = {}
    for option in options:
      if option[0] == "/":
        try:
          del pieopts[option[1:]]
        except KeyError:
          pass
      else:
        adds[option] = options[option]

    pieopts.update(adds)

  # source file management
  def checkSources(self, name):
    if not name in self._pie["collections"]:
      raise PieError("invalid collection")
    # todo: include into this check a check for files that can be handled by our workflow
    for path in self._pie["collections"][name]["paths"]:
      if not os.path.exists(path):
        raise PieError("invalid path")

  def addSources(self, name, paths):
    if not name in self._pie["collections"]:
      self._pie["collections"][name] = { "paths": {}, "options": {} }

    piePaths = self._pie["collections"][name]["paths"]

    for path in paths:
      path = os.path.abspath(path)
      if not path in piePaths:
        piePaths[path] = {}

  def removeSources(self, name, paths=None):
    if not name in self._pie["collections"]:
      raise PieError("source collection not listed.")
    if not paths:
      del self._pie["collections"][name]
      return 1
    else:
      piePaths = self._pie["collections"][name]["paths"]
      for path in paths:
        path = os.path.abspath(path)
        if path in piePaths:
          del piePaths[path]
        # ignore if paths are not in here
      return 2

  def getSources(self):
    return self._pie["collections"]

