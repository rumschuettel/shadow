#!/usr/bin/env python

import optparse
from glob import glob
import os
import shutil
import ctypes
from distutils.spawn import find_executable as findExe
import sys
import re

#import curses
#import ui
from pretty import pretty

import tempfile
import piefile
from piefile import PieError

# defines
VERSION = .7
__HERE__ = os.path.dirname(__file__)

# helper functions
def error(msg):
  print(pretty(msg, "bright red"))
def success(msg):
  print(pretty(msg, "bright green"))

def basename(path):
  return os.path.splitext(os.path.basename(path))[0]
def tempfilename():
  return basename(tempfile.mktemp())

SetConsoleTitle = ctypes.windll.kernel32.SetConsoleTitleW

# executables
from executables import executables, execute, ExeError

# profiles
from profiles import profiles

# print program version and check executables
def info(show=True):
  print(pretty("shadow.pie", "bright purple"), "version", VERSION, "(c) 2012")
  print()

  i = 0
  for exe in executables:
    try:
      print(exe)
      process = execute(exe, "version")
      version = process.stdout.readline().rstrip()
      show and print(pretty(version, "bright gray"))
      executables[exe]["available"] = version
      process.wait()

      show and print(pretty("using " + findExe(exe), "bright gray"))
      i += 1
    except Exception as e:
      error(str(e))
      show and print(exe, "not available")

  if i == len(executables.keys()):
    show and success("shadow.pie can find all executables.")
  else:
    show and error("could not find all executables. shadow.pie will not work correctly.")
    return 1

  print()
  print("loaded profiles:", " ".join(list(profiles.keys())))


# create new project
def new():
  try:
    pie = piefile.Piefile(True)
  except PieError as e:
    error(str(e))
    return 1
  success("created piefile. project ready.")
  return 0

# add source to project
def add(name, paths):
  piefile.Piefile().addSources(name, paths)
  success("ok")
  return 0

def remove(name, paths):
  try:
    piefile.Piefile().removeSources(name, paths)
  except PieError as e:
    error(str(e))
    return 1

  if len(paths) == 0:
    success("removed collection.")
  else:
    success("removed paths from collections.")
  return 0

# add  and remove temporary paths to project
def addTemp(path):
  try:
    piefile.Piefile().addPong(path)
  except PieError as e:
    error(str(e))
    return 1
  success("ok")
  return 0

def removeTemp(path):
  try:
    piefile.Piefile().removePong(path)
  except PieError as e:
    error(str(e))
    return 1
  success("ok")
  return 0

def remove(name, paths):
  try:
    piefile.Piefile().removeSources(name, paths)
  except PieError as e:
    error(str(e))
    return 1

  if len(paths) == 0:
    success("removed collection.")
  else:
    success("removed paths from collections.")
  return 0

def show(collection_filter, verbose):
  try:
    pie = piefile.Piefile()
  except PieError as e:
    error(str(e))
    return 1
  print("user-defined temporary paths:")
  if len(pie.get("pongs")) == 0:
    print(pretty("none", "dark gray"))
  for pong in pie.get("pongs"):
    print(pretty(pong, "dark gray"))

  sources = pie.getSources()
  for collection in sources:
    # only show specified collections
    skip = not not collection_filter
    for f in collection_filter:
      if f in collection:
        skip = False
        break
    if skip:
      continue
    
    print(pretty(collection, "bold"))
    if "profile" in sources[collection]:
      profile = sources[collection]["profile"]
      print("profile:", profile)

      options = list(profiles[profile]["defaults"].keys())
      options.sort()
      for option in options:
        if option in sources[collection]["options"]:
          print(option, "=", sources[collection]["options"][option], pretty(profiles[profile]["defaults"][option], "dark gray"))
        else:
          print(option, "=", pretty(profiles[profile]["defaults"][option], "dark gray"))
      # invalid options
      for option in set(sources[collection]["options"]) - set(options):
        print(pretty("!", "bright red"), option, "=", sources[collection]["options"][option]);

      if verbose:
        paths = list(sources[collection]["paths"].keys())
        paths.sort()
        for path in paths:
          pathoptions = sources[collection]["paths"][path]
          print(pretty(path, "dark gray"))
          for option in options:
            if option in pathoptions:
              print(option, "=", pathoptions[option], pretty(profiles[profile]["defaults"][option], "dark gray"))
          # invalid options
          for option in set(pathoptions) - set(options):
            print(pretty("!", "bright red"), option, "=", pathoptions[option]);

      else:
        print(pretty("%d files" % len(sources[collection]["paths"]), "dark gray"))
    else:
      print(pretty("profile not set", "bright yellow"))

  return 0

def setProfile(name, profile):
  try:
    piefile.Piefile().setProfile(name, profile)
  except PieError as e:
    error(str(e))
    return 1
  success("ok")
  return 0

def setOptions(name, options, file=None):
  try:
    piefile.Piefile().setOptions(name, options, file)
  except PieError as e:
    error(str(e))
    return 1
  success("ok")
  return 0

def getFormat(path):
  proc = execute("ffmbc", "analyse", {"in": path})
  proc.wait()
  for line in proc.stdout.readlines():
    m = re.search('Video:.+?(\d+)x(\d+)([ip]).+?(\d+\.\d+)\s+?fps', line)
    if m:
      return {
        "width": int(m.group(1)),
        "height": int(m.group(2)),
        "mode": m.group(3),
        "fps": float(m.group(4))
      }
      return False

from time import sleep

def make(collections_whitelist=[], files_whitelist=[], threads=1):
  pie = piefile.Piefile()
  collections = pie.getSources()

  for path in pie.get("pongs"):
    if not os.path.exists(path):
      error("invalid temporary path: " + path)
      return 1

  temppaths = [tempfile.gettempdir()] + pie.get("pongs")
  # return pong path
  class Pong:
    def __init__(self, path):
      self.prefix = basename(path)
      self.curpathindex = 0
    def pong(self, ext, tiny=False):
      if not tiny:
        self.curpathindex += 1
        if self.curpathindex == len(temppaths):
          self.curpathindex = 0

      return os.path.abspath(temppaths[self.curpathindex] + "/" + self.prefix + tempfilename() + "." + ext)

  # print factory
  class Factory:
    def __init__(self, print):
      self.print = print

    def run(self, exe, command, args):
      print(pretty(exe, "bright blue"), command)
      proc = execute(exe, command, args)

      for line in proc.stdout:
        line = line.strip()
        self.print(line)

      return proc.wait()

  thread = 0
  for collection_name in collections:
    # skip collections that are not in list
    if collections_whitelist:
      skip = True
      for collection in collections_whitelist:
        if collection.lower() in collection_name.lower():
          skip = False
          break
      if skip:
        print(pretty("skipping entire collection %s" % collection_name, "cyan"))
        continue
    collection = collections[collection_name]

    # get profile and paths
    if not "profile" in collection:
      error("no profile set")
      continue
    profile = collection["profile"]
    if not profile in profiles:
      error(profile + " is not a valid profile.")
      continue
    stage = profiles[profile]["stage"]
    options = profiles[profile]["defaults"].copy()
    options.update(collection["options"])
    sources = collection["paths"]

    # create output directory
    if not os.path.exists(pie.get("target") + collection_name):
      try:
        os.mkdir(pie.get("target") + collection_name)
      except OSError as e:
        error(str(e))
        continue

    for source in sources:
      source = os.path.abspath(source)
      # skip files that are not in list
      if files_whitelist:
        skip = True
        for file in files_whitelist:
          if file.lower() in source.lower():
            skip = False
            break
        if skip:
          print(pretty("skipping %s" % os.path.basename(source), "cyan"))
          continue

      SetConsoleTitle("processing " + collection_name + "/" + os.path.basename(source))
      info = getFormat(source)
      if info == False:
        error("could not analyse " + os.path.basename(source))
        continue
      print("%(width)dx%(height)d/%(fps).2f%(mode)s" % info)
      info.update(options)
      info.update(sources[source])

      pong = Pong(source)
      factory = Factory(print)

      cursource = source
      failure = False
      for item in stage:
        try:
          (res, newsource) = item["f"](factory.run, cursource, pong.pong, info)
        except ExeError as e:
          error(str(e))
          res = -12

        if cursource != source and cursource != newsource:
          # remove intermediate file and stuff like .ffindex
          for path in glob(cursource + "*"):
            print(pretty("clean up", "cyan"), path)
            try:
              os.unlink(path)
              success("ok")
            except:
              error("not removed")
        cursource = newsource
        if res != 0:
          failure = True
          error(cursource + " stuck. try to clean up...")
          input("wait...")
          try:
            os.unlink(newsource)
            success("ok")
          except:
            error(newsource + " not removed")
          break
      if failure:
        break

      # move to output
      target = pie.get("target") + collection_name + "/" + basename(source) + "" + os.path.splitext(cursource)[1]
      if os.path.exists(target):
        os.unlink(target)
      try:
        shutil.move(cursource, target)
      except OSError as e:
        error(str(e))
        break

      success("done")

def main(argv = None):
  parser = optparse.OptionParser(usage="usage: %prog [options] [FILES]")
  parser.add_option("-n", "--new", default=False, help="create new project in current directory", dest="new", action="store_true")
  parser.add_option("-t", "--temp", default=False, help='modifies temporary targets, ACTION can be either "add" or "remove"', metavar="ACTION", dest="temp", action="store")
  parser.add_option("-a", "--add", help="add source files FILES to collection COLLECTION", dest="add", action="store", metavar="COLLECTION")
  parser.add_option("-r", "--remove", help="removes files FILES from collection COLLECTION from sources. If no file is given, removes the whole collection", dest="remove", action="store", metavar="COLLECTION")
  parser.add_option("-s", "--show", default=False, help="show all collections", dest="show", action="store_true")
  group = optparse.OptionGroup(parser, "Profile")
  group.add_option("-p", "--profile", help="set profile for collection COLLECTION", dest="profile", action="store", metavar="COLLECTION")
  group.add_option("-o", "--options", help="set options (given as key=value or /key) for collection COLLECTION", dest="options", action="store", metavar="COLLECTION")
  parser.add_option_group(group)

  parser.add_option("-f", "--file", help="set options only for file FILE, or transcode only file FILE", dest="file", action="append", metavar="FILE")

  parser.add_option("-m", "--make", default=False, help="start encoding process", dest="make", action="store_true")
  parser.add_option("-v", "--verbose", default=False, help="show more output", dest="verbose", action="store_true")

  opts, args = parser.parse_args()

  if opts.new:
    return new()
  elif opts.temp:
    if len(args) != 1:
      error("specify exactly one path.")
      return 1
    if opts.temp == "add":
      return addTemp(args[0])
    elif opts.temp == "remove":
      return removeTemp(args[0])
    else:
      error('action should be "add" or "remove".')
      return 1
  elif opts.add:
    # glob wildcards
    globbed = []
    for path in args:
      globbed += glob(path)

    return add(opts.add, globbed)
  elif opts.remove:
    # glob wildcards
    globbed = []
    for path in args:
      if glob(path):
        globbed += glob(path)
      else:
        globbed += path

    # make sure deleting the collection is intentional
    if not globbed:
      while True:
        key = input("delete collection " + opts.remove + "? (y or n) ")
        if key in "yYjJ":
          return remove(opts.remove, globbed)
        elif key in "nN":
          return 0
        else:
          continue

    return remove(opts.remove, globbed)
  elif opts.show:
    return show(args, opts.verbose)
  elif opts.profile:
    if len(args) != 1:
      error("no profile specified.")
      return 1
    if args[0] not in profiles:
      error("invalid profile specified.")
      return 1
    return setProfile(opts.profile, args[0])
  elif opts.options:
    if len(args) == 0:
      error("no options specified")
      return 1
    parsed = {}
    for option in args:
      pair = option.split("=")
      if len(pair) == 1:
        parsed.update({pair[0]: True})
      elif len(pair) == 2:
        try:
          parsed.update({pair[0]: eval(pair[1])})
        except:
          error("could not parse option " + pair[0])
      else:
        error("could not parse option " + pair[0])
        return 1

    if opts.file:
      for file in opts.file:
        setOptions(opts.options, parsed, file)
    else:
      return setOptions(opts.options, parsed)
  elif opts.make:
    if opts.file:
      make(args, opts.file)
    else:
      return make(args)
  else:
    return info()

if __name__ == "__main__":
  sys.exit(main())
